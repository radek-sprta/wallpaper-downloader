#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Set National Geographic photo of the day as background
#
# Copyright (C) 2017
# Lorenzo Carbonell Cerezo <lorenzo.carbonell.cerezo@gmail.com>
# Radek Sprta <mail@radeksprta.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies:
# python3-cssselect

import getopt
import os
import shutil
import sys

import requests
from lxml.html import fromstring
from gi.repository import Gio
from gi.repository import GLib


URL = 'http://www.nationalgeographic.com/photography/photo-of-the-day/'


def usage():
    usage = """
    Usage:
    wallpaper-downloader.py [-OPTIONS]

    Options:
    -r, --revert              revert to the previous wallpaper
    """
    print(usage)


def backup():
    """
    Backup wallpaper.
    """
    new = get_wallpaper_path()
    old = os.path.join(os.path.dirname(new), 'old.jpg')
    try:
        save(new, old)
        print('Saved old wallpaper to {}'.format(old))
    except FileNotFoundError:
        print('No wallpapers to backup')


def get_image_url():
    """
    Get url of the National Geography Picture of the Day

    Returns:
        (str): Image URL.
    """
    resp = requests.get(URL)
    if resp.status_code == 200:
        doc = fromstring(resp.text)
        for meta in doc.cssselect('meta'):
            prop = meta.get('property')
            if prop == 'og:image':
                return meta.get('content')


def get_options():
    """
    Return a dictionary of command line options.

    Returns:
        (`dict`): Command line options
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:], "r", ["revert"])
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    options = {}
    for opt, arg in opts:
        if opt in ('-r', '--revert'):
            options['revert'] = True

    return options


def get_wallpaper_path():
    """
    Returns path to wallpaper.

    Returns:
        (str): Path to wallpaper.
    """
    default_data_path = os.path.expanduser('~/.local/share')
    data_path = os.getenv('XDG_DATA_HOME', default_data_path)
    wallpaper_path = os.path.join(data_path, 'wallpaper-downloader')

    try:
        os.makedirs(wallpaper_path)
    except FileExistsError:
        pass

    return os.path.join(wallpaper_path, 'wallpaper.jpg')


def revert():
    """
    Revert to previous wallpaper.
    """
    new = get_wallpaper_path()
    old = os.path.join(os.path.dirname(new), 'old.jpg')
    try:
        save(old, new)
        print('Reverted to previous wallpaper')
    except FileNotFoundError:
        print('No old wallpaper found')


def save(source, dest):
    """
    Save a picture.

    Args:
        source(str): Source path.
        dest(str): Destination path.
    """
    shutil.copy(source, dest)


def set_background(afile=None):
    if os.environ.get("GNOME_DESKTOP_SESSION_ID"):
        set_picture_uri('org.gnome.desktop.background',
                        'picture-uri', afile, True)
    elif os.environ.get("DESKTOP_SESSION") == "mate":
        set_picture_uri('org.mate.background', 'picture-filename', afile)


def set_picture_uri(branch, key, filename, uri=False):
    """
    Save the picture path in a gsettings key.

    Args:
        branch: Gsettings branch.
        key: Background uri key.
        value: Path to wallpaper.
    """
    gso = Gio.Settings.new(branch)
    if afile and os.path.exists(afile):
        if uri:
            variant = GLib.Variant('s', 'file://%s' % (afile))
        else:
            variant = GLib.Variant('s', afile)
        gso.set_value(key, variant)


def set_national_geographic_wallpaper():
    image_url = get_image_url()
    print('URL: {}'.format(image_url))
    resp = requests.get(image_url, stream=True)
    print('Status: {}'.format(resp.status_code))
    if resp.status_code == 200:
        try:
            backup()
            with open(get_wallpaper_path(), 'wb') as f:
                for chunk in resp.iter_content(1024):
                    f.write(chunk)
            set_background(get_wallpaper_path())
        except Exception as e:
            print(e)


def main():
    """
    Run the script.
    """
    options = get_options()
    if 'revert' in options:
        revert()
        set_background()
    else:
        set_national_geographic_wallpaper()
    sys.exit(0)


if __name__ == '__main__':
    main()
